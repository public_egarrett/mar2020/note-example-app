import React, { useState, useCallback, useEffect } from 'react';
import './App.css';

const App = () => {
  //State constants: each note contains a body and title value
  const [list, setList] = useState([]);
  const [activeNoteBody, setActiveNoteBody] = useState("");
  const [activeNoteIndex, setActiveNoteIndex] = useState(0);

  /**
   * Setup on initial load - add new, blank note.
   */
  useEffect(() => {
    //Add first note
    const newNote = { body: "", title: "Note #1" };
    setList([...list, newNote]);
  }, []);

  /**
   * Adds a new entry to the list of notes.
   * 
   * @param list
   * @param setList
   * @param body
   */
  const useUpdateList = (list, setList) => useCallback(() => {
    //First persist current note
    list[activeNoteIndex].body = activeNoteBody;

    var title = "Note #".concat(list.length + 1);

    const newNote = { body: "", title };

    setList([...list, newNote]);
  }, [list, setList]);

  //Used to populate ul based on list of note items
  const populatedList = list.map((item, index) => 
    <li key={index} className={index} 
      onClick={(e) => 
        updateNoteDisplay(e)
      }>
      {item.title}
    </li>
  );

  /**
   * Updates currently displayed note based on selection from <ul>.
   * Uses the <li> element's className.
   */
  const updateNoteDisplay = (e) => {
    //First persist current note
    list[activeNoteIndex].body = activeNoteBody;

    //Then update display
    setActiveNoteIndex(Number(e.target.className));
    setActiveNoteBody(list[Number(e.target.className)].body);  
  }

  return(
    <div className="App">
      <div id="notecontainer">
        <div id="notebody">
          <div id="buttondiv">
            <button id="addbtn" onClick={useUpdateList(list, setList, activeNoteBody)}>Add Note</button>
          </div>
          <div id="notecontents">
            <h1 id="notetitle">Note #{(activeNoteIndex + 1)}</h1>
            <textarea id="notetext" value={activeNoteBody} onChange={(e) => setActiveNoteBody(e.target.value)}></textarea>
          </div>
        </div>
        <div id="notelistcontainer">
          <ul id="notelist">
            {populatedList}
          </ul>
        </div>
      </div>
    </div>
  );
}

export default App;
