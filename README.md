This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

A basic notepad application demonstrating management of state using hooks and functional elements. Allows for adding and editing of notes within a session; no persistence.